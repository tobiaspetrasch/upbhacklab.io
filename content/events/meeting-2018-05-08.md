---
title: Weekly Meeting
location: Freiraum HNI
start: 2018-05-08T18:15:00+02:00
duration: 105
---

Treffen am Dienstag, da Donnerstag ein Feiertag ist.

<hr>

Anwesend: 15

* Organisatorisches
* Vorstellung gelöster CTFs
