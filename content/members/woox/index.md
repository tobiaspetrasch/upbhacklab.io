---
title: Tobias Petrasch
nick: woox
links:
- GitHub: https://github.com/tobiaspetrasch
- Mail: mailto:petrasch@mail.uni-paderborn.de
- Web: http://www.tobiaspetrasch.de
resources:
- name: avatar
  src: woox.jpg
---
