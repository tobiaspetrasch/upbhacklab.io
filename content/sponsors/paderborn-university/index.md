---
title: "Paderborn University"
link: http://www.uni-paderborn.de/
weight: 1
resources:
- name: logo
  src: upb.png
---

The university allows us to use their rooms and provides us with resources like virtual machines.
